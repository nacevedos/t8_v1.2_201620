package mundo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import modelos.Estacion;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoNoDirigido;
import estructuras.Nodo;

/**
 * Clase que representa el administrador del sistema de metro de New York
 * @param <K>
 *
 */
public class Administrador<K> {

	/**
	 * Ruta del archivo de estaciones
	 */
	public static final String RUTA_PARADEROS ="./data/stations.txt";

	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS ="./data/routes.txt";


	/**
	 * Grafo que modela el sistema de metro de New York
	 */
	//TODO Declare el atributo grafo No dirigido que va a modelar el sistema. 
	// Los identificadores de las estaciones son String
	private GrafoNoDirigido< String, String> grafo;


	/**
	 * Construye un nuevo administrador del Sistema
	 */
	public Administrador() 
	{
		//TODO inicialice el grafo como un GrafoNoDirigido
		grafo = new GrafoNoDirigido<>();
	}

	/**
	 * Devuelve todas las rutas que pasan por la estacion con nombre dado
	 * @param identificador 
	 * @return Arreglo con los identificadores de las rutas que pasan por la estacion requerida
	 */
	public String[] darRutasEstacion(String identificador)
	{
		//TODO Implementar
		Arco<String, String> destino [] = grafo.darArcosDestino(identificador);
		Arco<String, String> origen [] = grafo.darArcosOrigen(identificador);
		String rutas [] = new String [destino.length + origen.length];
		int i = 0;
		for (Arco<String, String> arcoDestino : destino) 
		{
			rutas[i++] = arcoDestino.darInformacion();
		}

		for (Arco<String, String> arcOrigen : origen) 
		{
			rutas[i++] = arcOrigen.darInformacion();
		}

		return rutas;
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones mas cercanas del sistema.
	 * @return distancia minima entre 2 estaciones
	 */
	public double distanciaMinimaEstaciones()
	{
		//TODO Implementar
		double d = 0;
		Arco<String, String>[] arcos = grafo.darArcos();
		for (Arco<String, String> arco : arcos) 
		{
			if (arco != null && arco.darCosto() < d)
			{
				d = arco.darCosto();
			}
		}

		return d;
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones más lejanas del sistema.
	 * @return distancia maxima entre 2 paraderos
	 */
	public double distanciaMaximaEstaciones()
	{
		//TODO Implementar
		double d = 0;
		Arco<String, String>[] arcos = grafo.darArcos();

		for (Arco<String, String> arco : arcos) 
		{
			if ( arco != null && arco.darCosto() > d )
			{
				d = arco.darCosto();
			}
		}

		return d;
	}


	/**
	 * Metodo encargado de extraer la informacion de los archivos y llenar el grafo 
	 * @throws Exception si ocurren problemas con la lectura de los archivos o si los archivos no cumplen con el formato especificado
	 */
	public void cargarInformacion() throws Exception
	{
		//TODO Implementar

		try 
		{
			BufferedReader bf= new BufferedReader(new FileReader(RUTA_PARADEROS));

			bf.readLine();

			String nextLine;

			while ((nextLine = bf.readLine()) != null ) 
			{
				String [] separado = nextLine.split(";");
				Comparable<K> id = (Comparable<K>) separado[0];
				Estacion e = new Estacion (id, Double.parseDouble(separado[1]), Double.parseDouble(separado[2]));
				grafo.agregarNodo(e);
			}
		} 
		catch (IOException e) 
		{

			e.printStackTrace();
		}


		System.out.println("Se han cargado correctamente "+grafo.darNodos().length+" Estaciones");

		//TODO Implementar
		try 
		{
			BufferedReader bf= new BufferedReader(new FileReader(RUTA_RUTAS));
			BufferedReader bf2= new BufferedReader(new FileReader(RUTA_RUTAS));

			bf.readLine();
			bf2.readLine();

			String nextLine;
			String nextLine1;
			String nextLine2;

			while ((nextLine = bf.readLine()) != null && (nextLine2 = bf2.readLine()) != null) 
			{
				String nombre = bf.readLine();
				String num = bf.readLine();
				String nombre2 = bf2.readLine();
				String num2 = bf2.readLine();

				int cuantas = Integer.parseInt(num);
				
				bf2.readLine();
				for (int i = 0; i < cuantas; i++) 
				{
					nextLine = bf.readLine();

					String [] arreglo1 = nextLine.split(" ");

					if(cuantas-1 > i)
					{
						nextLine2 = bf2.readLine();
						if(nextLine2 == null || nextLine2 == " " || nextLine == null || nextLine == " "){
							break;
						}
						String [] arreglo2 = nextLine2.split(" ");
						
						//System.out.println(separado2[1]);
						
						int distancia = Integer.parseInt(arreglo2[1]);
						grafo.agregarArco(arreglo1[0], arreglo2[0], distancia);
					}
					
				}
			}
		} 
		catch (IOException e) 
		{

			e.printStackTrace();
		}

		System.out.println("Se han cargado correctamente "+ grafo.darArcos().length+" arcos");
	}

	/**
	 * Busca la estacion con identificador dado<br>
	 * @param identificador de la estacion buscada
	 * @return estacion cuyo identificador coincide con el parametro, null de lo contrario
	 */
	public Estacion<String> buscarEstacion(String identificador)
	{
		//TODO Implementar
		return (Estacion<String>) grafo.buscarNodo(identificador);
	}

}
