package estructuras;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javafx.print.Collation;

/**
 * Clase que representa un grafo no dirigido con pesos en los arcos.
 * @param K tipo del identificador de los vertices (Comparable)
 * @param E tipo de la informacion asociada a los arcos

 */
public class GrafoNoDirigido<K extends Comparable<K>, E> implements Grafo<K,E> {

	/**
	 * Nodos del grafo
	 */
	//TODO Declare la estructura que va a contener los nodos
	private LinearProbingHashST<K, Nodo<K>> nodes;

	/**
	 * Lista de adyacencia 
	 */
	private HashMap<K, LinkedList<Arco<K,E>>> adj;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
	// Es libre de implementarlo con la representacion de su agrado. 

	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido() {
		//TODO implementar
		nodes = new LinearProbingHashST<>();
		adj = new HashMap<>();
	}

	@Override
	public boolean agregarNodo(Nodo<K> nodo) {
		//TODO implementar
		if(nodo != null)
		{
			nodes.put(nodo.darId(), nodo);
			return true;
		}
		return false;
	}

	@Override
	public boolean eliminarNodo(K id) {
		//TODO implementar
		if(id != null)
		{
			nodes.delete(id);
			return true;
		}
		return false;
	}

	@Override
	public Arco<K,E>[] darArcos() {
		//TODO implementar
		int contador = 0;
		Arco<K,E>[] arcoCon = new Arco[12524];
		Collection<LinkedList<Arco<K, E>>> c = adj.values();

		Iterator<LinkedList<Arco<K, E>>> i = c.iterator();
		while(i.hasNext())
		{
			LinkedList<Arco<K, E>> a = i.next();
			Iterator<Arco<K, E>> it = a.iterator();
			while(it.hasNext())
			{
				Arco<K, E> arco = it.next();
				arcoCon[contador] = arco;
				contador++;
			}
		}
		return arcoCon;
	}

	private Arco<K,E> crearArco( K inicio, K fin, double costo, E e )
	{
		Nodo<K> nodoI = buscarNodo(inicio);
		Nodo<K> nodoF = buscarNodo(fin);
		if ( nodoI != null && nodoF != null)
		{
			return new Arco<K,E>( nodoI, nodoF, costo, e);
		}
		{
			return null;
		}
	}

	@Override
	public Nodo<K>[] darNodos() {
		//TODO implementar
		Nodo<K> [] nodoCon = new Nodo[nodes.size()];
		int i = 0;
		//TODO implementar
		for (K nodo : nodes.keys()) 
		{
			nodoCon [i]= nodes.get(nodo);
		}
		return nodoCon;
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo, E obj) {
		//TODO implementar


		Arco<K, E> nuevo= crearArco(inicio, fin, costo, obj);
		LinkedList<Arco<K,E>> huevo= adj.get(inicio);
		if ( huevo == null)
		{
			huevo= new LinkedList<Arco<K,E>>(); 
			huevo.addLast(nuevo);
			adj.put(inicio, huevo);
		}
		else adj.get(inicio).addLast(nuevo);


		return true;
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo) {
		return agregarArco(inicio, fin, costo, null);
	}

	@Override
	public Arco<K,E> eliminarArco(K inicio, K fin) {
		//TODO implementar

		Arco<K, E> actual = null;
		LinkedList<Arco<K, E>> lista =  adj.get(inicio);	
		for (int i = 0; i<lista.getSize(); i++)
		{
			actual =  lista.get(i);
			if (fin.equals(actual.darNodoFin().darId()))
			{
				lista.remove(actual);
				break;
			}
		}
		return actual;
	}

	@Override
	public Nodo<K> buscarNodo(K id) {
		//TODO implementar
		return (Nodo<K>) nodes.get(id);
	}

	@Override
	public Arco<K,E>[] darArcosOrigen(K id) {
		//TODO implementar
		LinkedList<Arco<K, E>> arcosO = adj.get(id);
		Arco<K,E>[] arcoCon = new Arco[arcosO.getSize()];
		int i = 0;
		for (Arco<K, E> arco : arcosO) 
		{
			arcoCon[i] = arco;
			i++;
		}

		return arcoCon;
	}

	@Override
	public Arco<K,E>[] darArcosDestino(K id) {
		//TODO implementar

		int cont = 0;
		Arco<K,E>[] arcoCon = new Arco[12524];
		Collection<LinkedList<Arco<K, E>>> collection = adj.values();

		Iterator<LinkedList<Arco<K, E>>> iterador = collection.iterator();
		while(iterador.hasNext())
		{
			LinkedList<Arco<K, E>> arc = iterador.next();

			Iterator<Arco<K, E>> it = arc.iterator();
			while(it.hasNext())
			{
				Arco<K, E> arco = it.next();

				if (arco.equals(id))
				{
					arcoCon[cont] = arco;
					cont++;
				}
				arcoCon[cont] = arco;
				cont++;
			}
		}
		return arcoCon;
	}

}
